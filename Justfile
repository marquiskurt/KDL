build-grammar:
    #!/bin/sh
    cp grammar/Makefile grammar/tree-sitter-kdl
    cd grammar/tree-sitter-kdl
    ../compile_parser.sh . "/Applications/Nova.app"
    cd ../../
    cp grammar/tree-sitter-kdl/libtree-sitter-kdl.dylib KDL.novaextension/Syntaxes
    cp grammar/tree-sitter-kdl/queries/*.scm KDL.novaextension/Queries
    codesign -s - KDL.novaextension/Syntaxes/libtree-sitter-kdl.dylib

clean-grammar:
    #!/bin/sh
    rm -rf build
    cd grammar/tree-sitter-kdl
    make clean