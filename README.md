# Cuddle

**Cuddle** provides syntax highlighting and symbols for the **Cuddly Document
Language** ([KDL](https://kdl.dev)) in Panic's [Nova](https://nova.app) editor.

![Screenshot of KDL highlighting in Nova](Screenshot.png)

## Language Support

Cuddle provides support for KDL in the following areas:

- Syntax highlighting
- Commenting and injections
- Basic symbol browsing

By default, any files with the `kdl` extension will automatically assume KDL syntax
highlighting. You can go to Nova's settings to add custom types to apply KDL syntax
highlighting to files you specify, or you can change the type directly on the file
itself.

## Build from source

**Required Tools**  
- A C toolchain (Xcode Command Line tools recommended)
- Tree-sitter CLI
- Just
- Git

Start by running `git clone --recursive` to clone this repository with its submodules.

Download [Panic's Make scripts for grammars][pmake] and copy the scripts into the
`grammar` directory.

In the root of this project, run `just build-grammar` to build the Tree-sitter library
and have it copied into the Nova extension package.

[pmake]: https://docs.nova.app/syntax-reference/build_script.zip

## License

Cuddle is licensed under the MIT License.

## Credits

Cuddle was made possible by the following libraries and tools:

- tree-sitter-kdl: https://github.com/amaanq/tree-sitter-kdl
- Parser Make scripts: https://docs.nova.app/syntax-reference/build_script.zip
- KDL specifications (and logo): https://github.com/kdl-org/kdl
