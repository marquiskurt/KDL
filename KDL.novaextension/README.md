<!--
👋 Hello! As Nova users browse the extensions library, a good README can help them understand what your extension does, how it works, and what setup or configuration it may require.

Not every extension will need every item described below. Use your best judgement when deciding which parts to keep to provide the best experience for your new users.

💡 Quick Tip! As you edit this README template, you can preview your changes by selecting **Extensions → Activate Project as Extension**, opening the Extension Library, and selecting "KDL" in the sidebar.

Let's get started!
-->

<!--
🎈 Include a brief description of the features your syntax extension provides. For example:
-->

**Cuddle** provides syntax highlighting and symbols for the **Cuddly Document Language** ([KDL](https://kdl.dev)).

<!--
🎈 It can also be helpful to include a screenshot or GIF showing your extension in action:
-->

![](https://gitlab.com/marquiskurt/KDL/-/raw/root/Screenshot.png)

## Language Support

Cuddle provides support for KDL in the following areas:

- Syntax highlighting
- Commenting and injections
- Basic symbol browsing

By default, any files with the `kdl` extension will automatically assume KDL syntax
highlighting. You can go to Nova's settings to add custom types to apply KDL syntax
highlighting to files you specify, or you can change the type directly on the file
itself.

## Credits

Cuddle was made possible by the following libraries and tools:

- tree-sitter-kdl: https://github.com/amaanq/tree-sitter-kdl
- Parser Make scripts: https://docs.nova.app/syntax-reference/build_script.zip
- KDL specifications (and logo): https://github.com/kdl-org/kdl
